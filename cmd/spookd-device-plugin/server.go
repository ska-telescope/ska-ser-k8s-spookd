package main

import (
	"fmt"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	k8s "k8s.io/api/core/v1"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
	"net"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

// DevicePluginServer implements the Kubernetes device plugin API for a single device type
type DevicePluginServer struct {
	// the name of the extended resource this device server manages
	resourceName k8s.ResourceName

	// The current set of devices of this resource type
	devices map[uuid.UUID]Device

	// Path to the unix socket to serve on, and the gRPC server object
	socket string
	server *grpc.Server

	// Channel on which to receive device updates. Closing this quits the server
	updates chan map[uuid.UUID]Device

	// closed when this server's work is finished
	done chan error

	// Derived from resourceName and used in when generating environment variables
	// describing allocated devices in Allocate
	envVarPrefix string
}

func NewWidgetDevicePluginServer(resourceName k8s.ResourceName) *DevicePluginServer {
	return &DevicePluginServer{
		done:         make(chan error),
		resourceName: resourceName,
		socket:       pluginapi.DevicePluginPath + "spookd-" + sanitisedResourceName(resourceName) + ".sock",
		updates:      make(chan map[uuid.UUID]Device),
		envVarPrefix: generateEnvVarPrefix(resourceName),
	}
}

func sanitisedResourceName(resourceName k8s.ResourceName) string {
	escapedDeviceType := string(resourceName)
	escapedDeviceType = strings.ReplaceAll(escapedDeviceType, ".", "")
	escapedDeviceType = strings.ReplaceAll(escapedDeviceType, "/", "_")
	return escapedDeviceType
}

func generateEnvVarPrefix(resourceName k8s.ResourceName) string {
	return strings.ToUpper(sanitisedResourceName(resourceName))
}

// dial establishes the gRPC communication with the registered device plugin.
func dial(endpoint string, timeout time.Duration) (*grpc.ClientConn, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	conn, err := grpc.DialContext(ctx, endpoint,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
		grpc.WithContextDialer(func(ctx context.Context, addr string) (net.Conn, error) {
			return (&net.Dialer{}).DialContext(ctx, "unix", addr)
		}),
	)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

// Serve starts the gRPC server of the device plugin
func (m *DevicePluginServer) Serve() error {
	m.server = grpc.NewServer()
	pluginapi.RegisterDevicePluginServer(m.server, m)

	socket, err := net.Listen("unix", m.socket)
	if err != nil {
		return err
	}

	go func() {
		defer close(m.done)
		defer m.cleanup()
		err := m.server.Serve(socket)
		if err != nil {
			log.Errorf("Error during gRPC Serve(): %s", err)
			m.done <- err
		}
		log.Debugf("%p: gRPC Serve() completed", m)
	}()

	// Wait for server to start by launching a blocking connection
	conn, err := dial(m.socket, 5*time.Second)
	if err != nil {
		return err
	}

	_ = conn.Close() // errors if already closing, so safely ignore

	return nil
}

// Register registers the device plugin for the given resourceName with Kubelet.
func (m *DevicePluginServer) Register(kubeletEndpoint, resourceName string) error {
	conn, err := dial(kubeletEndpoint, 5*time.Second)
	if err != nil {
		return err
	}

	//goland:noinspection GoUnhandledErrorResult
	defer conn.Close() // errors if already closing, so safely ignore

	client := pluginapi.NewRegistrationClient(conn)
	reqt := &pluginapi.RegisterRequest{
		Version:      pluginapi.Version,
		Endpoint:     path.Base(m.socket),
		ResourceName: resourceName,
	}

	_, err = client.Register(context.Background(), reqt)
	if err != nil {
		return err
	}
	return nil
}

// ListAndWatch pulls successive sets of devices off the updates channel
// and streams them to the kubelet.
func (m *DevicePluginServer) ListAndWatch(_ *pluginapi.Empty, s pluginapi.DevicePlugin_ListAndWatchServer) error {
	defer func() {
		// ListAndWatch is only called once by the kubelet,
		// so we want to end the server when it exits
		go m.server.GracefulStop()
	}()

	for m.devices = range m.updates {
		resp := pluginapi.ListAndWatchResponse{}
		for _, dev := range m.devices {
			resp.Devices = append(
				resp.Devices,
				&pluginapi.Device{ID: dev.UUID.String(), Health: pluginapi.Healthy},
			)
		}

		log.Debugf("%p: reporting %d %s devices", m, len(resp.Devices), m.resourceName)
		err := s.Send(&resp)
		if err != nil {
			return err
		}
	}

	log.Debugf("%p: exiting ListAndWatch for %s", m, m.resourceName)
	return nil
}

// Allocate which return list of devices.
func (m *DevicePluginServer) Allocate(_ context.Context, reqs *pluginapi.AllocateRequest) (*pluginapi.AllocateResponse, error) {
	responses := pluginapi.AllocateResponse{}

	for _, req := range reqs.ContainerRequests {
		log.Debugf("%p: request IDs: %v", m, req.DevicesIDs)
		envVars := make(map[string]string)
		devNames := make([]string, 0, len(req.DevicesIDs))

		for i, id := range req.DevicesIDs {
			if device, ok := m.devices[uuid.MustParse(id)]; ok {
				envPrefix := fmt.Sprintf("%s_DEV%d_", m.envVarPrefix, i)
				envVars[envPrefix+"TYPE"] = string(device.ResourceName)
				envVars[envPrefix+"ID"] = device.InstanceID
				for key, val := range device.Env {
					envVars[envPrefix+key] = val
				}
				if len(req.DevicesIDs) == 1 {
					envPrefix := fmt.Sprintf("%s_", m.envVarPrefix)
					envVars[envPrefix+"TYPE"] = string(device.ResourceName)
					envVars[envPrefix+"ID"] = device.InstanceID
					for key, val := range device.Env {
						envVars[envPrefix+key] = val
					}
				}
			} else {
				return nil, fmt.Errorf("invalid allocation request: unknown device: %s", id)
			}
		}
		envVars[m.envVarPrefix+"_NUM_DEVICES"] = strconv.Itoa(len(req.DevicesIDs))
		log.Debugf("Devices to be offered: %v/%v", req.DevicesIDs, devNames)

		response := pluginapi.ContainerAllocateResponse{Envs: envVars}

		responses.ContainerResponses = append(responses.ContainerResponses, &response)
	}

	return &responses, nil
}

func (m *DevicePluginServer) GetDevicePluginOptions(context.Context, *pluginapi.Empty) (*pluginapi.DevicePluginOptions, error) {
	return &pluginapi.DevicePluginOptions{PreStartRequired: false, GetPreferredAllocationAvailable: false}, nil
}

func (m *DevicePluginServer) GetPreferredAllocation(context.Context, *pluginapi.PreferredAllocationRequest) (*pluginapi.PreferredAllocationResponse, error) {
	return &pluginapi.PreferredAllocationResponse{}, nil
}

func (m *DevicePluginServer) PreStartContainer(context.Context, *pluginapi.PreStartContainerRequest) (*pluginapi.PreStartContainerResponse, error) {
	return &pluginapi.PreStartContainerResponse{}, nil
}

func (m *DevicePluginServer) cleanup() {
	if err := os.Remove(m.socket); err != nil && !os.IsNotExist(err) {
		log.Errorf("Error during cleanup: %s", err)
	}
}

// Start starts the gRPC server and register the device plugin to Kubelet
func (m *DevicePluginServer) Start() error {
	m.cleanup()

	err := m.Serve()
	if err != nil {
		return fmt.Errorf("could not start device server: %w", err)
	}
	log.Infof("%p: Starting to serve on %s", m, m.socket)

	err = m.Register(pluginapi.KubeletSocket, string(m.resourceName))
	if err != nil {
		log.Errorf("Could not register device plugin: %v", err)
		m.server.Stop()
		return err
	}
	log.Infof("%p: Registered device plugin with Kubelet", m)

	return nil
}

func (m *DevicePluginServer) MustServe() {
	err := m.Start()
	if err != nil {
		log.Error(err)
		panic("Could not contact Kubelet! Did you enable the device plugin feature gate?")
	}
}
