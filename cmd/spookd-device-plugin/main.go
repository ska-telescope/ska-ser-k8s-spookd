package main

import (
	"flag"
	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
	"os"
	"strings"
	"syscall"
	"time"
)

func main() {
	var cfg PluginConfig

	// Parse command-line arguments
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	flagLogLevel := flag.String("log-level", "info", "Logging level: error, info or debug")
	flagConfigFile := flag.String("config-file", "", "Path to device configuration YAML file")
	flagPollInterval := flag.Duration("poll-interval", 10*time.Second, "How often to poll config-file, in seconds")
	flagConfigMap := flag.String("config-map", "", "Get device configuration from '<ConfigMap name>:<key>'")
	flagHostname := flag.String("hostname", "", "hostname to identify as; leave blank to autodetect")
	flag.Parse()

	switch *flagLogLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	}

	if (*flagConfigFile == "") == (*flagConfigMap == "") {
		log.Errorf("Exactly one of --config-file and --config-map must be provided")
		os.Exit(1)

	}

	if *flagConfigFile != "" {
		cfg.configFile.path = *flagConfigFile
		cfg.configFile.pollInterval = *flagPollInterval
		log.Debugf("Resource config file path: %s", cfg.configFile.path)
	}

	if *flagConfigMap != "" {
		var found bool
		cmName, cmKey, found := strings.Cut(*flagConfigMap, ":")
		if !found {
			log.Errorf("--config-map '%s' does not conform to <ConfigMap name>:<key>", *flagConfigMap)
			os.Exit(1)
		}
		cfg.configMap.Name, cfg.configMap.Key = cmName, cmKey
	}

	if *flagHostname == "" {
		hostname, err := os.Hostname()
		if err != nil {
			log.Fatalf("Fatal error getting Hostname: %s", err)
			os.Exit(1)
		}
		cfg.hostname = hostname
	} else {
		cfg.hostname = *flagHostname
	}

	log.Println("Starting device plugin socket watcher.")
	watcher, err := newFSWatcher(pluginapi.DevicePluginPath)
	if err != nil {
		log.Printf("Failed to created FS watcher: %s.", err)
		os.Exit(1)
	}
	defer watcher.Close()

	log.Println("Starting OS watcher.")
	signals := newOSWatcher(syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	var pluginDirector *DevicePluginDirector

loop:
	for quit := false; !quit; {

		if pluginDirector == nil {
			log.Infof("Starting plugin director")
			pluginDirector = NewDevicePluginDirector(cfg)
		}

		select {
		case <-pluginDirector.done:
			log.Infof("Plugin director stopped unexpectedly")
			break loop

		case event := <-watcher.Events:
			if event.Name == pluginapi.KubeletSocket && event.Op&fsnotify.Create == fsnotify.Create {
				log.Infof("%s created, restarting", pluginapi.KubeletSocket)
			} else {
				continue
			}

		case err := <-watcher.Errors:
			log.Warnf("inotify: %s", err)
			continue

		case s := <-signals:
			switch s {
			case syscall.SIGHUP:
				log.Infof("Received SIGHUP; restarting")
			default:
				// pretty sure this is only ever going to run in unix environments
				log.Infof("Received %s; shutting down", unix.SignalName(s.(syscall.Signal)))
				quit = true
			}
		}

		log.Infof("Stopping plugin director")
		pluginDirector.Stop()
		log.Infof("Plugin director stopped")
		pluginDirector = nil
	}

	log.Infof("Exiting")
}
