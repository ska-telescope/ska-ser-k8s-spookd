package set

import (
	"golang.org/x/exp/maps"
	"testing"
)

func TestFromSlice(t *testing.T) {
	s := FromSlice([]int{1, 2, 3, 2})
	want := map[int]struct{}{1: {}, 2: {}, 3: {}}
	if !maps.Equal(s, want) {
		t.Errorf("FromSlice failed, got: %v, want: %v.", s, want)
	}
}

func TestFromMap(t *testing.T) {
	s := FromMap(map[int]string{1: "some", 2: "meaningless", 3: "values"})
	want := map[int]struct{}{1: {}, 2: {}, 3: {}}
	if !maps.Equal(s, want) {
		t.Errorf("FromMap failed, got: %v, want: %v.", s, want)
	}
}

func TestNewSet(t *testing.T) {
	s := NewSet(1, 2, 3)
	want := map[int]struct{}{1: {}, 2: {}, 3: {}}
	if !maps.Equal(s, want) {
		t.Errorf("NewSet failed, got: %v, want: %v.", s, want)
	}
}

func TestAddRemove(t *testing.T) {
	var want Set[int]

	s := make(Set[int])
	s.Add(1)
	s.Add(2)
	s.Add(3)
	s.Add(2)
	want = NewSet(1, 2, 3)
	if !maps.Equal(s, want) {
		t.Errorf("Add failed, got: %v, want: %v.", s, want)
	}

	s.Remove(2)
	s.Remove(1)
	want = NewSet(3)
	if !maps.Equal(s, want) {
		t.Errorf("Remove failed, got: %v, want: %v.", s, want)
	}
}

func TestContains(t *testing.T) {
	s := Set[int]{1: {}}
	if s.Contains(1) == false {
		t.Errorf("s.Contains(1) should be true: %v", s)
	}
	if s.Contains(2) == true {
		t.Errorf("s.Contains(2) should be false: %v", s)
	}
}

func TestDifference(t *testing.T) {
	s := NewSet(1, 2).Difference(NewSet(2, 3))
	want := NewSet(1)
	if !maps.Equal(s, want) {
		t.Errorf("Difference failed, got: %v, want: %v.", s, want)
	}
}

func TestIntersection(t *testing.T) {
	s := NewSet(1, 2).Intersection(NewSet(2, 3))
	want := NewSet(2)
	if !maps.Equal(s, want) {
		t.Errorf("Intersection failed, got: %v, want: %v.", s, want)
	}
}

func TestUnion(t *testing.T) {
	s := NewSet(1, 2).Union(NewSet(2, 3))
	want := NewSet(1, 2, 3)
	if !maps.Equal(s, want) {
		t.Errorf("Union failed, got: %v, want: %v.", s, want)
	}
}

func TestEquals(t *testing.T) {

	tests := []struct {
		name string
		a    Set[int]
		b    Set[int]
		want bool
	}{
		{"equal", NewSet(1, 2, 3), NewSet(1, 2, 3), true},
		{"unequal", NewSet(1, 2, 3), NewSet(1, 2, 4), false},
		{"superset", NewSet(1, 2, 3), NewSet(1, 2, 3, 4), false},
		{"subset", NewSet(1, 2, 3), NewSet(1, 2), false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.Equals(tt.b); got != tt.want {
				t.Errorf("Equals() = %v, want %v", got, tt.want)
			}
		})
	}
}
